package dk.s4.hl7.validation;

import org.junit.Test;

import dk.s4.hl7.cda.convert.QFDDXmlCodec;
import dk.s4.hl7.cda.convert.decode.SetupQFDDDocumentForTest;
import dk.s4.hl7.cda.convert.encode.qfdd.SetupQFDDAnalogSliderExample;
import dk.s4.hl7.cda.convert.encode.qfdd.SetupQFDDDiscreteSliderExample;
import dk.s4.hl7.cda.convert.encode.qfdd.SetupQFDDMultipleChoiceExample;
import dk.s4.hl7.cda.convert.encode.qfdd.SetupQFDDNumericExample;
import dk.s4.hl7.cda.convert.encode.qfdd.SetupQFDDPrecondition;
import dk.s4.hl7.cda.convert.encode.qfdd.SetupQFDDTextExample;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import generated.CdaType;

public class TestValidateQFDDExamples extends BaseTest<QFDDDocument> {

  public TestValidateQFDDExamples() {
    super(new QFDDXmlCodec());
  }

  @Test
  public void testPrecondition() throws Exception {
    validateDocument(new SetupQFDDPrecondition().createDocument(), CdaType.QFDD);
  }

  @Test
  public void testAnalogSlider() throws Exception {
    validateDocument(new SetupQFDDAnalogSliderExample().createDocument(), CdaType.QFDD);
  }

  @Test
  public void testDiscreteSlider() throws Exception {
    validateDocument(new SetupQFDDDiscreteSliderExample().createDocument(), CdaType.QFDD);
  }

  @Test
  public void testMultipleChoice() throws Exception {
    validateDocument(new SetupQFDDMultipleChoiceExample().createDocument(), CdaType.QFDD);
  }

  @Test
  public void testNumeric() throws Exception {
    validateDocument(new SetupQFDDNumericExample().createDocument(), CdaType.QFDD);
  }

  @Test
  public void testText() throws Exception {
    validateDocument(new SetupQFDDTextExample().createDocument(), CdaType.QFDD);
  }

  @Test
  public void testQFDDForTest() throws Exception {
    validateDocument(SetupQFDDDocumentForTest.defineAsCDA(), CdaType.QFDD);
  }
}