package dk.s4.hl7.cda.convert.decode.apd;

import dk.s4.hl7.cda.model.apd.Status;
import dk.s4.hl7.cda.model.apd.StatusV10;

public class StatusV10Impl {
  public static Status valueOf(String status) {
    return StatusV10.valueOf(status);
  }

  public static StatusV10 valueOf(Status appointmentStatus) {
    if (appointmentStatus instanceof StatusV10) {
      return (StatusV10) appointmentStatus;
    }
    throw new IllegalStateException(
        "Invalid Appointment status " + appointmentStatus + ". Wrong version of Status - expected version 1.0");
  }
}
