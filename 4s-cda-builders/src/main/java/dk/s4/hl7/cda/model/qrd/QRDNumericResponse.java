package dk.s4.hl7.cda.model.qrd;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.model.BuilderException;
import dk.s4.hl7.cda.model.util.ModelUtil;

/**
 * Numeric response where the user have provided an numeric answer
 * 
 * The range is given by the minimum and maximum values.
 * The type of the answer value is given by intervalType
 * The answer is defined by value and type
 */
public class QRDNumericResponse extends QRDResponse {
  private static final Logger logger = LoggerFactory.getLogger(QRDNumericResponse.class);

  private BasicType type;
  private String value;
  private IntervalType intervalType;
  private String minimum; // low
  private String maximum; // high

  private static final Map<IntervalType, BasicType> mapIntervalTypeToBasicType = createMapIntervalTypeToBasicType();

  private static Map<IntervalType, BasicType> createMapIntervalTypeToBasicType() {
    Map<IntervalType, BasicType> map = new HashMap<IntervalType, BasicType>();
    map.put(IntervalType.IVL_INT, BasicType.INT);
    map.put(IntervalType.IVL_REAL, BasicType.REAL);
    map.put(IntervalType.IVL_TS, BasicType.TS);
    return map;
  }

  /**
   * "Effective Java" Builder for constructing QRDNumericResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QRDNumericResponseBuilder
      extends QRDResponse.BaseQRDResponseBuilder<QRDNumericResponse, QRDNumericResponseBuilder> {

    public QRDNumericResponseBuilder() {
    }

    private BasicType type;
    private String value;
    private IntervalType intervalType;
    private String minimum; // head value
    private String maximum; // denominator

    public QRDNumericResponseBuilder setValue(String value, BasicType type) {
      ModelUtil.checkNull(value, "Value is mandatory");
      ModelUtil.checkNull(type, "Value type is mandatory");
      this.value = value;
      this.type = type;
      if (type != null && value != null && type == BasicType.REAL) {
        this.value = value.replace(',', '.');
      }
      return this;
    }

    public QRDNumericResponseBuilder setInterval(String minimum, String maximum, IntervalType intervalType) {
      ModelUtil.checkNull(minimum, "Minimum value is mandatory");
      ModelUtil.checkNull(maximum, "Maximum value is mandatory");
      ModelUtil.checkNull(intervalType, "Interval type is mandatory");
      this.intervalType = intervalType;
      this.minimum = ModelUtil.checkDots(minimum, intervalType);
      this.maximum = ModelUtil.checkDots(maximum, intervalType);
      return this;
    }

    @Override
    public QRDNumericResponse build() {
      if (value == null || type == null) {
        throw new BuilderException("One of the mandatory attributes 'value' or 'type' are missing");
      }
      matchTypes();
      return new QRDNumericResponse(this);
    }

    private void matchTypes() {
      if (intervalType != null) {
        BasicType mappedType = mapIntervalTypeToBasicType.get(intervalType);
        if (type != mappedType) {
          logger.warn("Type of value and interval type must match. type[" + type.name() + "], intervaltype["
              + intervalType.name() + "]");
        }
      }
    }

    @Override
    public QRDNumericResponseBuilder getThis() {
      return this;
    }
  }

  private QRDNumericResponse(QRDNumericResponseBuilder builder) {
    super(builder);
    type = builder.type;
    value = builder.value;
    intervalType = builder.intervalType;
    minimum = builder.minimum;
    maximum = builder.maximum;
  }

  public BasicType getType() {
    return type;
  }

  public String getTypeAsString() {
    if (type != null) {
      return type.name();
    }
    return null;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public IntervalType getIntervalType() {
    return intervalType;
  }

  public String getIntervalTypeAsString() {
    if (intervalType != null) {
      return intervalType.name();
    }
    return null;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }

}
