package dk.s4.hl7.cda.convert.encode;

import java.io.IOException;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.QuestionnaireEntity;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

public class BuildUtil {

  public static void buildTemplateIds(XmlStreamBuilder xmlBuilder, String... templateIds) throws IOException {
    for (String templateId : templateIds) {
      xmlBuilder.element("templateId").attribute("root", templateId).elementShortEnd();
    }
  }

  public static void buildTemplateIdWithExtension(XmlStreamBuilder xmlBuilder, String templateId, String extension)
      throws IOException {
    xmlBuilder.element("templateId").attribute("root", templateId).attribute("extension", extension).elementShortEnd();
  }

  public static void buildTemplateIds(XmlStreamBuilder xmlBuilder, List<String> templateIds) throws IOException {
    for (String templateId : templateIds) {
      xmlBuilder.element("templateId").attribute("root", templateId).elementShortEnd();
    }
  }

  public static void buildId(String root, String authorityName, String extension, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (extension != null && root != null) {
      xmlBuilder.element("id").attribute("root", root).attribute("extension", extension);
      if (authorityName != null) {
        xmlBuilder.attribute("assigningAuthorityName", authorityName);
      }
      xmlBuilder.elementShortEnd();
    } else {
      buildNullFlavor("id", xmlBuilder);
    }
  }

  public static void buildId(ID id, XmlStreamBuilder xmlBuilder) throws IOException {
    if (id != null && id.getExtension() != null && id.getRoot() != null) {
      xmlBuilder.element("id").attribute("root", id.getRoot()).attribute("extension", id.getExtension());
      if (id.getAuthorityName() != null) {
        xmlBuilder.attribute("assigningAuthorityName", id.getAuthorityName());
      }
      xmlBuilder.elementShortEnd();
    } else {
      buildNullFlavor("id", xmlBuilder);
    }
  }

  public static void buildId(String root, String extension, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("id").attribute("root", root).attribute("extension", extension).elementShortEnd();
  }

  public static void buildNullFlavor(String elementName, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element(elementName).attribute("nullFlavor", "NI").elementShortEnd();
  }

  public static void buildQuestionnaireEntityCode(QuestionnaireEntity questionnaireEntity, XmlStreamBuilder xmlBuilder)
      throws IOException {
    CodedValue code = questionnaireEntity.getCode();
    if (code != null) {
      xmlBuilder
          .element("code")
          .attribute("code", code.getCode())
          .attribute("displayName", code.getDisplayName())
          .attribute("codeSystem", code.getCodeSystem())
          .attribute("codeSystemName", code.getCodeSystemName());
      xmlBuilder.element("originalText").value(questionnaireEntity.getQuestion()).elementEnd();
      xmlBuilder.elementEnd();
    }
  }

  public static void buildCode(String code, String codeSystem, String displayName, String codeSystemName,
      XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("code").attribute("code", code).attribute("codeSystem", codeSystem);
    if (displayName != null) {
      xmlBuilder.attribute("displayName", displayName);
    }
    if (codeSystemName != null) {
      xmlBuilder.attribute("codeSystemName", codeSystemName);
    }
    xmlBuilder.elementShortEnd();
  }

  public static void buildCode(String code, String codeSystem, XmlStreamBuilder xmlBuilder) throws IOException {
    buildCode(code, codeSystem, null, null, xmlBuilder);
  }

  public static void buildCode(String code, String codeSystem, String displayName, XmlStreamBuilder xmlBuilder)
      throws IOException {
    buildCode(code, codeSystem, displayName, null, xmlBuilder);
  }

  public static void buildCode(CodedValue codedValue, XmlStreamBuilder xmlBuilder) throws IOException {
    buildCode(codedValue.getCode(), codedValue.getCodeSystem(), codedValue.getDisplayName(),
        codedValue.getCodeSystemName(), xmlBuilder);
  }

  public static void buildNullFlavorableCodeTranslated(CodedValue codedValue, CodedValue translation,
      XmlStreamBuilder xmlBuilder) throws IOException {
    buildCodeWithoutEnd("code", codedValue.getCode() != null ? "code" : "nullFlavor",
        codedValue.getCode() != null ? codedValue.getCode() : "NI", codedValue.getCodeSystem(),
        codedValue.getDisplayName(), codedValue.getCodeSystemName(), xmlBuilder);
    if (translation != null) {
      buildCodeWithoutEnd("translation", translation.getCode() != null ? "code" : "nullFlavor",
          translation.getCode() != null ? translation.getCode() : "NI", translation.getCodeSystem(),
          translation.getDisplayName(), translation.getCodeSystemName(), xmlBuilder);
      xmlBuilder.elementShortEnd(); // end translation
      xmlBuilder.elementEnd(); // end code
    } else {
      xmlBuilder.elementShortEnd(); // end code
    }
  }

  public static void buildNullFlavorableCode(CodedValue codedValue, XmlStreamBuilder xmlBuilder) throws IOException {
    buildCodeWithoutEnd("code", codedValue.getCode() != null ? "code" : "nullFlavor",
        codedValue.getCode() != null ? codedValue.getCode() : "NI", codedValue.getCodeSystem(),
        codedValue.getDisplayName(), codedValue.getCodeSystemName(), xmlBuilder);
    xmlBuilder.elementShortEnd();
  }

  private static void buildCodeWithoutEnd(String elementName, String codeAttributeName, String codeValue,
      String codeSystemValue, String displayNameValue, String codeSystemNameValue, XmlStreamBuilder xmlBuilder)
      throws IOException {
    xmlBuilder.element(elementName).attribute(codeAttributeName, codeValue).attribute("codeSystem", codeSystemValue);
    if (displayNameValue != null) {
      xmlBuilder.attribute("displayName", displayNameValue);
    }
    if (codeSystemNameValue != null) {
      xmlBuilder.attribute("codeSystemName", codeSystemNameValue);
    }
  }

  public static void buildCodedValue(XmlStreamBuilder xmlBuilder, CodedValue answer) throws IOException {
    if (answer.getCode() != null) {
      xmlBuilder.attribute("code", answer.getCode());
    }
    if (answer.getCodeSystem() != null) {
      xmlBuilder.attribute("codeSystem", answer.getCodeSystem());
    }
    if (answer.getDisplayName() != null) {
      xmlBuilder.attribute("displayName", answer.getDisplayName());
    }
    if (answer.getCodeSystemName() != null) {
      xmlBuilder.attribute("codeSystemName", answer.getCodeSystemName());
    }
  }

  public static void buildNamedCodedValued(XmlStreamBuilder xmlBuilder, String elementName, CodedValue codedValue)
      throws IOException {
    if (codedValue != null) {
      xmlBuilder.element(elementName);
      if (codedValue.getCode() != null) {
        xmlBuilder.attribute("code", codedValue.getCode());
      }
      if (codedValue.getCodeSystem() != null) {
        xmlBuilder.attribute("codeSystem", codedValue.getCodeSystem());
      }
      if (codedValue.getDisplayName() != null) {
        xmlBuilder.attribute("displayName", codedValue.getDisplayName());
      }
      if (codedValue.getCodeSystemName() != null) {
        xmlBuilder.attribute("codeSystemName", codedValue.getCodeSystemName());
      }
      xmlBuilder.elementShortEnd(); // End coded value
    }
  }
}
