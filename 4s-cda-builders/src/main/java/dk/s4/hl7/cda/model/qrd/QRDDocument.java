package dk.s4.hl7.cda.model.qrd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.core.BaseClinicalDocument;

/**
 * Represents a QRD document by sections
 */
public class QRDDocument extends BaseClinicalDocument {
  private List<Section<QRDResponse>> sections;
  private CodedValue questionnaireType;

  public QRDDocument(ID id) {
    super(HL7.CDA_TYPEID_ROOT, HL7.CDA_TYPEID_EXTENSION, id, Loinc.QRD_CODE, Loinc.QRD_DISPLAYNAME,
        new String[] { MedCom.DK_QRD_ROOT_OID, MedCom.DK_QRD_ROOT_OID_LEVEL }, HL7.REALM_CODE_DK);
    this.sections = new ArrayList<Section<QRDResponse>>();
  }

  public List<Section<QRDResponse>> getSections() {
    return sections;
  }

  public void addSection(Section<QRDResponse> section) {
    sections.add(section);
  }

  public CodedValue getQuestionnaireType() {
    return questionnaireType;
  }

  public void setQuestionnaireType(CodedValue questionnaireType) {
    this.questionnaireType = questionnaireType;
  }

}