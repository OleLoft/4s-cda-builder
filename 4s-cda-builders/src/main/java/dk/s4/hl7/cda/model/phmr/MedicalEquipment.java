package dk.s4.hl7.cda.model.phmr;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.CdaBuilderException;

/** Medical equipment class. * 
 */
public final class MedicalEquipment {
  private String medicalDeviceCode;
  private String medicalDeviceDisplayName;
  private String manufacturerModelName;
  private String softwareName;
  private String serialNumber;
  private String serialNumberType;

  /** The Builder for creating instances of a measurement.*/
  public static class MedicalEquipmentBuilder {

    private String serialNumber = null;
    private String serialNumberType = null;
    private String medicalDeviceCode = null;
    private String medicalDeviceDisplayName = null;
    private String manufacturerModelName = null;
    private String softwareName = null;

    /** Build the measurement based upon the given parameters.
     * 
     * @return the final measurement.
     */
    public MedicalEquipment build() {
      validateWellformednessAndThrowExceptionIfNot();
      if (serialNumber == null || serialNumberType == null) {
        return new MedicalEquipment(medicalDeviceCode, medicalDeviceDisplayName, manufacturerModelName, softwareName);
      }
      return new MedicalEquipment(medicalDeviceCode, medicalDeviceDisplayName, manufacturerModelName, softwareName,
          serialNumber, serialNumberType);
    }

    public void validateWellformednessAndThrowExceptionIfNot() {
      if (manufacturerModelName == null) {
        throw new CdaBuilderException("The MedicalEquipment's manufacturer model name is null");
      }
      if (softwareName == null) {
        throw new CdaBuilderException("The MedicalEquipment's software name is null");
      }
    }

    public MedicalEquipmentBuilder setMedicalDeviceCode(String theDeviceCode) {
      medicalDeviceCode = theDeviceCode;
      return this;
    }

    public MedicalEquipmentBuilder setMedicalDeviceDisplayName(String theDisplayName) {
      medicalDeviceDisplayName = theDisplayName;
      return this;
    }

    public MedicalEquipmentBuilder setManufacturerModelName(String theModelName) {
      manufacturerModelName = theModelName;
      return this;
    }

    public MedicalEquipmentBuilder setSoftwareName(String theSoftwareName) {
      softwareName = theSoftwareName;
      return this;
    }

    public MedicalEquipmentBuilder setSerialNumber(String serialNumber) {
      this.serialNumber = serialNumber;
      return this;
    }

    public MedicalEquipmentBuilder setSerialNumberType(String serialNumberType) {
      this.serialNumberType = serialNumberType;
      return this;
    }
  }

  /** Medical equipment constructor.
   * 
   * @param deviceCode The device code.
   * @param deviceDisplayName The device display name.
   * @param modelName The model name.
   * @param softwareName The software name.
   */
  private MedicalEquipment(String deviceCode, String deviceDisplayName, String modelName, String softwareName) {
    this.medicalDeviceCode = deviceCode;
    this.medicalDeviceDisplayName = deviceDisplayName;
    this.manufacturerModelName = modelName;
    this.softwareName = softwareName;
    this.serialNumber = medicalDeviceCode;
    this.serialNumberType = MedCom.ROOT_AUTHORITYNAME;
  }

  private MedicalEquipment(String deviceCode, String deviceDisplayName, String modelName, String softwareName,
      String serialNumber, String serialNumberType) {
    this.medicalDeviceCode = deviceCode;
    this.medicalDeviceDisplayName = deviceDisplayName;
    this.manufacturerModelName = modelName;
    this.softwareName = softwareName;
    this.serialNumber = serialNumber;
    this.serialNumberType = serialNumberType;
  }

  /** Get the medical device code.
   * @return The medical device code.
   */
  public String getMedicalDeviceCode() {
    return medicalDeviceCode;
  }

  /** Get the medical device display name.
   * @return The medical device display name.
   */
  public String getMedicalDeviceDisplayName() {
    return medicalDeviceDisplayName;
  }

  /** Get the manufacturer model name.
   * @return The manufacturer model name.
   */
  public String getManufacturerModelName() {
    return manufacturerModelName;
  }

  /** Get the software name.
   * @return The software name.
   */
  public String getSoftwareName() {
    return softwareName;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public String getSerialNumberType() {
    return serialNumberType;
  }

  /** Return string representation of this
   * instance.
   * @return the string representation
   */
  public String toString() {
    String value;
    value = "Medical Equipment: " + getMedicalDeviceCode() + ", " + getMedicalDeviceDisplayName() + ", "
        + getManufacturerModelName() + ", " + getSoftwareName();
    return value;
  }
}
