package dk.s4.hl7.cda.convert.decode.v11;

import dk.s4.hl7.cda.model.apd.Status;
import dk.s4.hl7.cda.model.apd.StatusV11;

public class StatusV11Impl {
  public static Status valueOf(String status) {
    return StatusV11.valueOf(status);
  }

  public static StatusV11 valueOf(Status appointmentStatus) {
    if (appointmentStatus instanceof StatusV11) {
      return (StatusV11) appointmentStatus;
    }
    throw new IllegalStateException(
        "Invalid Appointment status " + appointmentStatus + ". Wrong version of Status - expected version 1.1");
  }
}
