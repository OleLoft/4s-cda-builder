package dk.s4.hl7.cda.model.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Patient;

public abstract class BaseClinicalDocument implements ClinicalDocument {
  // realmCode (0..1 and not 0..n as in schema)
  protected String realmCode = null;
  // typeId (1..1) (mandatory)
  protected String typeIdRoot = null;
  // -
  protected String typeIdExtension = null;
  // templateId (0..n)
  protected String[] templateIds = null;
  // id (1..1) (mandatory)
  protected ID id = null;
  // code (1..1) (mandatory)
  protected String code = null;
  // -
  protected String codeDisplayName = null;
  // title (0..1)
  protected String title = null;
  // effectiveTime (1..1) (mandatory)
  protected Date effectiveTime = null;
  // confidentialityCode (1..1) (mandatory)
  protected String confidentialityCode = null;
  // languageCode (0..1)
  protected String languageCode = null;
  // setId (0..1)
  protected String setId = null;
  // versionNumber (0..1)
  protected Integer versionNumber = null;
  // copyTime (0..1) // * NOT USED
  // recordTarget (1..1 and not 1..n as in schema)
  protected Patient patient = null;
  // author (1..1 and not 1..n as in schema)
  protected Participant author;
  // dataEnterer (0..1)
  protected Participant dataenterer;
  // informant (0..n) // * NOT USED
  // custodian (1..1) (mandatory)
  protected OrganizationIdentity custodianOrganization = null;
  // informationRecipient (0..n)
  protected List<Participant> informationRecipents = null;
  // legalAuthenticator (0..1)
  protected Participant legalAuthenticator;
  // authenticator (0..n) // * NOT USED
  // participant (0..n)
  protected List<Participant> participants = null;
  // inFulfillmentOf (0..n) // * NOT USED
  // documentationOf (0..n) // Needed to make AdhocQuery for XDS registry
  private Date serviceStartTime;
  private Date serviceStopTime;
  // relatedDocument (0..n) // * NOT USED
  // authorization (0..n) // * NOT USED
  // componentOf (0..1) // * NOT USED

  public BaseClinicalDocument(String typeIdRoot, String typeIdExtension, ID id, String codeCode,
      String codeDisplayName) {
    this(typeIdRoot, typeIdExtension, id, codeCode, codeDisplayName, null, null);
  }

  public BaseClinicalDocument(String typeIdRoot, String typeIdExtension, ID id, String codeCode, String codeDisplayName,
      String[] templateIds) {
    this(typeIdRoot, typeIdExtension, id, codeCode, codeDisplayName, templateIds, null);
  }

  public BaseClinicalDocument(String typeIdRoot, String typeIdExtension, ID id, String code, String codeDisplayName,
      String[] templateIds, String realmCode) {
    if (typeIdRoot == null)
      throw new IllegalArgumentException("typeIdRoot");
    if (typeIdExtension == null)
      throw new IllegalArgumentException("typeIdExtension");

    this.typeIdRoot = typeIdRoot;
    this.typeIdExtension = typeIdExtension;
    setId(id);
    this.code = code;
    this.codeDisplayName = codeDisplayName;
    this.templateIds = templateIds;
    this.realmCode = realmCode;
    this.informationRecipents = new ArrayList<Participant>();
    this.participants = new ArrayList<Participant>();
  }

  @Override
  public String getRealmCode() {
    return realmCode;
  }

  @Override
  public String getTypeIdRoot() {
    return typeIdRoot;
  }

  @Override
  public String getTypeIdExtension() {
    return typeIdExtension;
  }

  @Override
  public String[] getTemplateIds() {
    return templateIds;
  }

  @Override
  public ID getId() {
    return id;
  }

  @Override
  public String getSetId() {
    return setId;
  }

  @Override
  public void setSetId(String setId) {
    this.setId = setId;
  }

  protected void setId(ID id) {
    if (id == null)
      throw new IllegalArgumentException("id is null");
    this.id = id;
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public String getCodeDisplayName() {
    return codeDisplayName;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public void setEffectiveTime(Date now) {
    if (now == null)
      throw new IllegalArgumentException("now");
    effectiveTime = now;
  }

  @Override
  public Date getEffectiveTime() {
    return effectiveTime;
  }

  @Override
  public String getConfidentialityCode() {
    return confidentialityCode;
  }

  @Override
  public void setConfidentialityCode(String code) {
    confidentialityCode = code;
  }

  @Override
  public String getLanguageCode() {
    return languageCode;
  }

  @Override
  public void setLanguageCode(String language) {
    languageCode = language;
  }

  @Override
  public void setDocumentVersion(String setId, Integer versionNumber) {
    this.setId = setId;
    this.versionNumber = versionNumber;
  }

  @Override
  public String getUniqueId() {
    return this.setId;
  }

  @Override
  public Integer getVersionNumber() {
    return versionNumber;
  }

  @Override
  public void setPatient(Patient patientIdentity) {
    this.patient = patientIdentity;
  }

  @Override
  public Patient getPatient() {
    return patient;
  }

  @Override
  public void setAuthor(Participant author) {
    this.author = author;
  }

  @Override
  public Participant getAuthor() {
    return author;
  }

  @Override
  public void setDataEnterer(Participant dataenterer) {
    this.dataenterer = dataenterer;
  }

  @Override
  public Participant getDataEnterer() {
    return dataenterer;
  }

  @Override
  public void setCustodian(OrganizationIdentity custodianIdentity) {
    this.custodianOrganization = custodianIdentity;
  }

  @Override
  public OrganizationIdentity getCustodianIdentity() {
    return custodianOrganization;
  }

  @Override
  public void setLegalAuthenticator(Participant legalAuthenticator) {
    this.legalAuthenticator = legalAuthenticator;
  }

  @Override
  public Participant getLegalAuthenticator() {
    return legalAuthenticator;
  }

  @Override
  public void setDocumentationTimeInterval(Date serviceStartTime, Date serviceStopTime) {
    this.serviceStartTime = serviceStartTime;
    this.serviceStopTime = serviceStopTime;
  }

  @Override
  public Date getServiceStartTime() {
    return serviceStartTime;
  }

  @Override
  public Date getServiceStopTime() {
    return serviceStopTime;
  }

  @Override
  public List<Participant> getParticipants() {
    return participants;
  }

  @Override
  public void setParticipants(List<Participant> participants) {
    this.participants = participants;
  }

  @Override
  public void addParticipant(Participant participant) {
    this.participants.add(participant);
  }

  @Override
  public List<Participant> getInformationRecipients() {
    return informationRecipents;
  }

  @Override
  public void setInformationRecipients(List<Participant> informationRecipents) {
    this.informationRecipents = informationRecipents;
  }

  @Override
  public void addInformationRecipients(Participant informationRecipent) {
    this.informationRecipents.add(informationRecipent);
  }
}