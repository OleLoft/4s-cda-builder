package dk.s4.hl7.cda.convert.base;

/**
 * The Codec<S, T> interface can be seen as an interface consolidating to symmetric converters into a single class 
 * for coding and decoding.  One converter will be used for the encode(...)
 * method and one will be used for the decode(...) method.
 * 
 * @see https://www.dartlang.org/articles/libraries/converters-and-codecs
 * 
 *  @author Frank Jacobsen Systematic
 */

public interface Codec<S, T> {
  T encode(S source);

  S decode(T source);
}
