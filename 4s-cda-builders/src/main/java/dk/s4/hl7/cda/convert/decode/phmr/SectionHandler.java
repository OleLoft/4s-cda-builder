package dk.s4.hl7.cda.convert.decode.phmr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlHandler;
import dk.s4.hl7.util.xml.XmlMapping;

public class SectionHandler implements CDAXmlHandler<PHMRDocument> {
  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section";
  public static final String COMPONENT_SECTION_CODE = "/ClinicalDocument/component/structuredBody/component/section/code";
  private static final Logger logger = LoggerFactory.getLogger(SectionHandler.class);

  private MeasurementHandler measurementHandler;
  private MedicalEquipmentHandler medicalEquipmentHandler;
  private XmlHandler currentHandler;

  public SectionHandler() {
    currentHandler = null;
    medicalEquipmentHandler = new MedicalEquipmentHandler();
    measurementHandler = new MeasurementHandler();
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code")) {
      String componentSectionCode = xmlElement.getAttributeValue("code");
      if (Loinc.SECTION_RESULTS_CODE.equalsIgnoreCase(componentSectionCode)) {
        measurementHandler.setComponentSectionCode(componentSectionCode);
        addHandlerToMap(xmlMapping, measurementHandler);
      } else if (Loinc.SECTION_VITAL_SIGNS_CODE.equalsIgnoreCase(componentSectionCode)) {
        measurementHandler.setComponentSectionCode(componentSectionCode);
        addHandlerToMap(xmlMapping, measurementHandler);
      } else if (Loinc.SECTION_MEDICAL_CODE.equalsIgnoreCase(componentSectionCode)) {
        addHandlerToMap(xmlMapping, medicalEquipmentHandler);
      } else {
        logger.warn("Ignoring unknown section with code " + componentSectionCode);
      }
    }
  }

  private void addHandlerToMap(XmlMapping xmlMapping, XmlHandler xmlhandler) {
    xmlhandler.addHandlerToMap(xmlMapping);
    currentHandler = xmlhandler;
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase("section")) {
      currentHandler.removeHandlerFromMap(xmlMapping);
      currentHandler = null;
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(COMPONENT_SECTION, this);
    xmlMapping.add(COMPONENT_SECTION_CODE, this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    // Ignore
  }

  @Override
  public void addDataToDocument(PHMRDocument phmrDocument) {
    medicalEquipmentHandler.addDataToDocument(phmrDocument);
    measurementHandler.addDataToDocument(phmrDocument);
  }
}
