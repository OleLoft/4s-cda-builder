package dk.s4.hl7.cda.convert.decode.general;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ReferenceRange;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class ObservationRangePatternHandler extends BaseXmlHandler {
  public static final String OBSERVATION_BASE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/referenceRange";
  private List<ReferenceRange> observationReferenceRanges;

  private String templateId;
  private CodedValue code;
  private String minimum;
  private boolean isMinimumInclusive;
  private String maximum;
  private boolean isMaximumInclusive;
  private String intervalType;

  public ObservationRangePatternHandler() {
    this.observationReferenceRanges = new ArrayList<ReferenceRange>();
    localClear();
    addPath(OBSERVATION_BASE);
    addPath(OBSERVATION_BASE + "/observationRange/templateId");
    addPath(OBSERVATION_BASE + "/observationRange/code");
    addPath(OBSERVATION_BASE + "/observationRange/value");
    addPath(OBSERVATION_BASE + "/observationRange/value/low");
    addPath(OBSERVATION_BASE + "/observationRange/value/high");
  }

  public List<ReferenceRange> getObservationReferenceRanges() {
    return observationReferenceRanges;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      String tempTemplateId = xmlElement.getAttributeValue("root");
      if (MedCom.DK_PHMR_OBSERVATION_RANGE_ROOT_OID.equalsIgnoreCase(tempTemplateId)) {
        templateId = tempTemplateId;
      } else {
        templateId = null;
      }
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code",
        "codeSystem")) {
      code = new CodedValue.CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      intervalType = xmlElement.getAttributeValue("type");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      minimum = xmlElement.getAttributeValue("value");
      String inclusiveValue = xmlElement.getAttributeValue("inclusive");
      isMinimumInclusive = inclusiveValue != null ? "true".equalsIgnoreCase(inclusiveValue) : true;
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      maximum = xmlElement.getAttributeValue("value");
      String inclusiveValue = xmlElement.getAttributeValue("inclusive");
      isMaximumInclusive = inclusiveValue != null ? "true".equalsIgnoreCase(inclusiveValue) : true;
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase("referenceRange")) {
      if (MedCom.DK_PHMR_OBSERVATION_RANGE_ROOT_OID.equalsIgnoreCase(templateId)
          && "IVL_PQ".equalsIgnoreCase(intervalType)) {
        observationReferenceRanges.add(new ReferenceRange.ReferenceRangeBuilder()
            .setCode(code)
            .setLowValue(minimum, isMinimumInclusive)
            .setHighValue(maximum, isMaximumInclusive)
            .build());
      }
      localClear();
    }
  }

  public void clear() {
    observationReferenceRanges.clear();
  }

  private void localClear() {
    templateId = null;
    code = null;
    minimum = null;
    isMinimumInclusive = false;
    maximum = null;
    isMaximumInclusive = false;
    intervalType = null;
  }

}
