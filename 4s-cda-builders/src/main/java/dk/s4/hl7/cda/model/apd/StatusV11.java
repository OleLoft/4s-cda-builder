package dk.s4.hl7.cda.model.apd;

public enum StatusV11 implements Status {
  /**
   * @since 1.0
   */
  ACTIVE,
  /**
   * @since 1.1
   */
  COMPLETED,
  /**
   * @since 1.1
   */
  ABORTED,
  /**
   * @since 1.1
   */
  SUSPENDED;
}
