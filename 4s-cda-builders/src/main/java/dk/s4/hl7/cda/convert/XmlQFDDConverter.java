package dk.s4.hl7.cda.convert;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ClinicalDocumentXmlConverter;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.header.CustodianHandler;
import dk.s4.hl7.cda.convert.decode.qfdd.SectionHandler;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;

/**
 * 
 * Parse QFDD xml to GreenCDA model
 *
 */
public class XmlQFDDConverter extends ClinicalDocumentXmlConverter<QFDDDocument> {
  public QFDDDocument convert(String source) {
    return deserialize(new StringReader(source));
  }

  @Override
  protected List<CDAXmlHandler<ClinicalDocument>> createParticipantHandlers() {
    List<CDAXmlHandler<ClinicalDocument>> list = new ArrayList<CDAXmlHandler<ClinicalDocument>>();
    list.add(createAuthorHandler());
    list.add(new CustodianHandler());
    return list;
  }

  @Override
  protected QFDDDocument createNewDocument(CDAHeaderHandler headerHandler) {
    return new QFDDDocument(headerHandler.getDocumentId());
  }

  @Override
  protected List<CDAXmlHandler<QFDDDocument>> createSectionHandlers() {
    List<CDAXmlHandler<QFDDDocument>> list = new ArrayList<CDAXmlHandler<QFDDDocument>>();
    list.add(new SectionHandler());
    return list;
  }
}