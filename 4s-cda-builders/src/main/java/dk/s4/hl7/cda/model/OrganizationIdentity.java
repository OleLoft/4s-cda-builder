package dk.s4.hl7.cda.model;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.codes.NSI;
import dk.s4.hl7.cda.model.AddressData.Use;

/**
 * Immutable object for information of an organization.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public final class OrganizationIdentity implements Locateable {
  private ID id;
  private String orgName;
  private Telecom[] telecomArray;
  private AddressData address;

  /**
   * "Effective Java" builder for constructing organization objects.
   * 
   * @author Henrik Baerbak Christensen, Aarhus University
   *
   */
  public static class OrganizationBuilder {
    // Required fields
    private ID id;
    // Optional fields
    private AddressData address = null;
    private String orgName = null;
    private Telecom[] telecom = null;
    // Temporaries
    private List<Telecom> teleComListTemporary;

    public OrganizationBuilder() {
      teleComListTemporary = new ArrayList<Telecom>();
    }

    public OrganizationBuilder setID(ID id) {
      this.id = id;
      return this;
    }

    public OrganizationBuilder setSOR(String sor) {
      this.id = NSI.createSOR(sor);
      return this;
    }

    public OrganizationBuilder setYDER(String yder) {
      this.id = NSI.createYDER(yder);
      return this;
    }

    public OrganizationBuilder setAddress(AddressData addr) {
      this.address = addr;
      return this;
    }

    public OrganizationBuilder setName(String orgName) {
      this.orgName = orgName;
      return this;
    }

    public OrganizationBuilder addTelecom(Use use, String protocol, String telecom) {
      teleComListTemporary.add(new Telecom(use, protocol, telecom));
      return this;
    }

    public OrganizationBuilder addTelecoms(List<Telecom> telecoms) {
      teleComListTemporary.addAll(telecoms);
      return this;
    }

    public OrganizationIdentity build() {
      OrganizationIdentity org;

      if (teleComListTemporary.size() > 0) {
        telecom = new Telecom[teleComListTemporary.size()];
        teleComListTemporary.toArray(telecom);
      } else {
        telecom = new Telecom[] {};
      }

      org = new OrganizationIdentity(this);
      return org;
    }
  }

  private OrganizationIdentity(OrganizationBuilder builder) {
    id = builder.id;
    orgName = builder.orgName;
    address = builder.address;
    telecomArray = builder.telecom;
  }

  /**
   * Get the organization OID.
   * 
   * @return The organization OID.
   */
  public String getIdValue() {
    if (id != null) {
      return id.getExtension();
    }
    return null;
  }

  public ID getId() {
    return id;
  }

  /**
   * Get the organization name.
   * 
   * @return The organization name.
   */
  public String getOrgName() {
    return orgName;
  }

  @Override
  public Telecom[] getTelecomList() {
    return telecomArray;
  }

  @Override
  public AddressData getAddress() {
    return address;
  }

  @Override
  public String toString() {
    String result = "Org: " + id.getExtension() + " ";
    result += " / " + orgName + " ";
    result += "Adr: " + address.toString() + " ";
    if (telecomArray != null) {
      result += "Telecom: [";
      for (Telecom tc : getTelecomList()) {
        result += tc + ",";
      }
      result += "] ";
    }
    return result;
  }
}
