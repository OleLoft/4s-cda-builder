package dk.s4.hl7.cda.model;

/**
 * Generic class representing a physical quantity interval
 */
public class PhysicalQuantityInterval extends Interval {
  private String unit;

  public String getUnit() {
    return unit;
  }

  public static class PhysicalQuantityIntervalBuilder extends IntervalBuilder {
    private String unit;

    public PhysicalQuantityInterval build() {
      return new PhysicalQuantityInterval(this);
    }

    public PhysicalQuantityIntervalBuilder setUnit(String unit) {
      this.unit = unit;
      return this;
    }
  }

  protected PhysicalQuantityInterval(PhysicalQuantityIntervalBuilder builder) {
    super(builder);
    this.unit = builder.unit;
  }

}
