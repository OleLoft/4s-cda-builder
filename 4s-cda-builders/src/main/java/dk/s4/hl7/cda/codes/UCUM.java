package dk.s4.hl7.cda.codes;

/**
 * A collection of UCUM unit strings
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public class UCUM {
  public static final String kg = "kg";
  public static final String mmHg = "mmHg";
  public static final String gPerL = "g/L";
  public static final String pct = "%";
  public static final String L = "L";
  public static final String NA = "NA";
}
