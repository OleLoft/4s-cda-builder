package dk.s4.hl7.cda.convert.decode.qrd;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.IdHandler;
import dk.s4.hl7.cda.convert.decode.general.QuestionOptionsPatternHandler;
import dk.s4.hl7.cda.convert.decode.general.ReferenceHandler;
import dk.s4.hl7.cda.convert.decode.general.ResponseReferenceRangePatternHandler;
import dk.s4.hl7.cda.convert.decode.general.ValueCollection;
import dk.s4.hl7.cda.convert.decode.general.ValueCollection.CE_INDEX;
import dk.s4.hl7.cda.convert.decode.general.ValueCollection.PQ_INDEX;
import dk.s4.hl7.cda.convert.decode.general.ValueCollection.ST_INDEX;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse.QRDAnalogSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse.QRDNumericResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDResponse.BaseQRDResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse.QRDTextResponseBuilder;
import dk.s4.hl7.util.xml.ElementAttributeFinder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class ObservationHandler extends BaseXmlHandler {
  public static final String OBSERVATION_SECTION = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation";

  private List<QRDResponse> responses;

  /**
   * There may be multiple template ids for the pattern. There's generally only
   * one template id but in the case of analog and discrete slider there are
   * two: analog or discrete slider oid and multiple choice oid
   */
  private Set<String> templates;
  private CodedValue questionCode;
  private String question;
  private ValueCollection valueCollection;

  private IdHandler idHandler;
  private ReferenceHandler referenceHandler;
  private QuestionOptionsPatternHandler questionOptionsPatternHandler;
  private ResponseReferenceRangePatternHandler responseReferenceRangePatternhandler;
  private ElementAttributeFinder elementAttributeFinder;

  public ObservationHandler() {
    idHandler = new IdHandler(OBSERVATION_SECTION);
    this.referenceHandler = new ReferenceHandler();
    this.questionOptionsPatternHandler = new QuestionOptionsPatternHandler();
    this.responseReferenceRangePatternhandler = new ResponseReferenceRangePatternHandler();
    this.elementAttributeFinder = createEntryRelationshipFinder();
    this.templates = new HashSet<String>();
    this.valueCollection = new ValueCollection();
    this.responses = new ArrayList<QRDResponse>();

    addPath(OBSERVATION_SECTION);
    addPath(OBSERVATION_SECTION + "/templateId");
    addPath(OBSERVATION_SECTION + "/code");
    addPath(OBSERVATION_SECTION + "/code/originalText");
    addPath(OBSERVATION_SECTION + "/value");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      templates.add(xmlElement.getAttributeValue("root"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code",
        "codeSystem")) {
      questionCode = new CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "originalText")) {
      question = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      valueCollection.addValue(xmlElement);
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "observation")) {
      String responseOid = determineObservationType();
      if (responseOid.equalsIgnoreCase(HL7.QRD_ANALOG_SLIDER_PATTERN_OID)) {
        createAnalogSliderResponse();
      } else if (responseOid.equalsIgnoreCase(HL7.QRD_DISCRETE_SLIDER_PATTERN_OID)) {
        createDiscreteSliderResponse();
      } else if (responseOid.equalsIgnoreCase(HL7.QRD_NUMERIC_PATTERN_OID)) {
        createNumericResponse();
      } else if (responseOid.equalsIgnoreCase(HL7.QRD_TEXT_QUESTION_PATTERN_OID)) {
        createTextResponse();
      } else if (responseOid.equalsIgnoreCase(HL7.QRD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID)) {
        createMultipleChoiceResponse();
      }
      localClear();
    }
  }

  private String determineObservationType() {
    String result = "";
    if (templates.size() == 1) {
      return templates.iterator().next();
    } else if (templates.size() > 1) {
      Iterator<String> itr = templates.iterator();
      while (itr.hasNext()) {
        String oid = itr.next();
        if (oid.equalsIgnoreCase(HL7.QRD_ANALOG_SLIDER_PATTERN_OID)
            || oid.equalsIgnoreCase(HL7.QRD_DISCRETE_SLIDER_PATTERN_OID)) {
          result = oid;
          break; // Analog slider and discrete slider overrules other oids
        }
        result = oid;
      }
    }
    return result;
  }

  private void createAnalogSliderResponse() {
    QRDAnalogSliderResponseBuilder builder = new QRDAnalogSliderResponseBuilder();
    builder.setCodeValue(questionCode).setId(idHandler.getId()).setQuestion(question);
    if (responseReferenceRangePatternhandler.hasValues() && !responseReferenceRangePatternhandler.isNumericResponse()) {
      builder.setInterval(responseReferenceRangePatternhandler.getMinimum(),
          responseReferenceRangePatternhandler.getMaximum(), responseReferenceRangePatternhandler.getIncrement());
    }
    List<String> value = valueCollection.getSingleValue();
    builder.setValue(value.get(PQ_INDEX.VALUE.ordinal()), value.get(PQ_INDEX.TYPE.ordinal()));
    addReferences(builder);
    responses.add(builder.build());
  }

  private void createDiscreteSliderResponse() {
    QRDDiscreteSliderResponseBuilder builder = new QRDDiscreteSliderResponseBuilder();
    builder.setCodeValue(questionCode).setId(idHandler.getId()).setQuestion(question);
    if (questionOptionsPatternHandler.hasValues()) {
      builder.setMinimum(questionOptionsPatternHandler.getMinimum());
    }
    List<String> value = valueCollection.getSingleValue();
    if (value != null && !value.isEmpty()) {
      builder.setAnswer(value.get(CE_INDEX.CODE.ordinal()), value.get(CE_INDEX.CODE_SYSTEM.ordinal()),
          value.get(CE_INDEX.DISPLAY_NAME.ordinal()), value.get(CE_INDEX.CODE_SYSTEM_NAME.ordinal()));
    }
    addReferences(builder);
    responses.add(builder.build());
  }

  private void createNumericResponse() {
    QRDNumericResponseBuilder builder = new QRDNumericResponseBuilder()
        .setCodeValue(questionCode)
        .setId(idHandler.getId())
        .setQuestion(question);
    if (responseReferenceRangePatternhandler.hasValues()) {
      builder.setInterval(responseReferenceRangePatternhandler.getMinimum(),
          responseReferenceRangePatternhandler.getMaximum(), responseReferenceRangePatternhandler.getIntervalType());
    }
    List<String> value = valueCollection.getSingleValue();
    builder.setValue(value.get(PQ_INDEX.VALUE.ordinal()), toBasicType(value.get(PQ_INDEX.TYPE.ordinal())));
    addReferences(builder);
    responses.add(builder.build());
  }

  private void createMultipleChoiceResponse() {
    QRDMultipleChoiceResponseBuilder builder = new QRDMultipleChoiceResponseBuilder();
    builder.setCodeValue(questionCode).setId(idHandler.getId()).setQuestion(question);
    if (questionOptionsPatternHandler.hasValues()) {
      builder.setInterval(questionOptionsPatternHandler.getMinimum(), questionOptionsPatternHandler.getMaximum());
    }
    List<List<String>> values = valueCollection.getValues();
    for (List<String> value : values) {
      if (value != null && !value.isEmpty()) {
        builder.addAnswer(value.get(CE_INDEX.CODE.ordinal()), value.get(CE_INDEX.CODE_SYSTEM.ordinal()),
            value.get(CE_INDEX.DISPLAY_NAME.ordinal()), value.get(CE_INDEX.CODE_SYSTEM_NAME.ordinal()));
      }
    }
    addReferences(builder);
    responses.add(builder.build());
  }

  private void createTextResponse() {
    QRDTextResponseBuilder builder = new QRDTextResponseBuilder();
    builder.setCodeValue(questionCode).setId(idHandler.getId()).setQuestion(question);
    List<String> value = valueCollection.getSingleValue();
    if (value != null && !value.isEmpty()) {
      builder.setText(value.get(ST_INDEX.TEXT.ordinal()));
    }
    addReferences(builder);
    responses.add(builder.build());
  }

  private BasicType toBasicType(String valueText) {
    return BasicType.valueOf(valueText.toUpperCase());
  }

  private void addReferences(BaseQRDResponseBuilder<?, ?> builder) {
    for (Reference reference : referenceHandler.getReferences()) {
      builder.addReference(reference);
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    idHandler.addHandlerToMap(xmlMapping);
    referenceHandler.addHandlerToMap(xmlMapping);
    responseReferenceRangePatternhandler.addHandlerToMap(xmlMapping);
    elementAttributeFinder.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    idHandler.removeHandlerFromMap(xmlMapping);
    referenceHandler.removeHandlerFromMap(xmlMapping);
    responseReferenceRangePatternhandler.removeHandlerFromMap(xmlMapping);
    elementAttributeFinder.removeHandlerFromMap(xmlMapping);
  }

  private ElementAttributeFinder createEntryRelationshipFinder() {
    ElementAttributeFinder elementAttributeFinder = new ElementAttributeFinder(
        OBSERVATION_SECTION + "/entryRelationship/observation/templateId", "templateId");
    elementAttributeFinder.addAttributeMatchers("root", HL7.QUESTION_OPTIONS_PATTERN_OBSERVATION_OID,
        questionOptionsPatternHandler);
    return elementAttributeFinder;
  }

  private void localClear() {
    templates.clear();
    questionCode = null;
    question = null;
    valueCollection.clear();
    idHandler.clear();
    referenceHandler.clear();
    questionOptionsPatternHandler.clear();
    responseReferenceRangePatternhandler.clear();
  }

  public List<QRDResponse> getResponses() {
    return responses;
  }

  public void clear() {
    localClear();
    responses.clear();
  }
}
