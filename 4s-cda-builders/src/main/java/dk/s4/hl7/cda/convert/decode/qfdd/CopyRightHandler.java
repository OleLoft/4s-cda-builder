package dk.s4.hl7.cda.convert.decode.qfdd;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class CopyRightHandler extends BaseXmlHandler {
  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section/entry/observation";

  private boolean isCopyRightSection;
  private String copyRightText;

  public CopyRightHandler() {
    addPath(COMPONENT_SECTION + "/templateId");
    addPath(COMPONENT_SECTION + "/value");
    isCopyRightSection = false;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      isCopyRightSection = HL7.QFDD_COPYRIGHT_PATTERN_TEMPLATEID.equalsIgnoreCase(xmlElement.getAttributeValue("root"));
    } else if (isCopyRightSection
        && ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      copyRightText = xmlElement.getElementValue();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

  public boolean isCopyRightSection() {
    return isCopyRightSection;
  }

  public String getCopyRightText() {
    return copyRightText;
  }

  public void clear() {
    this.isCopyRightSection = false;
    this.copyRightText = null;
  }

}
