package dk.s4.hl7.cda.convert;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ClinicalDocumentXmlConverter;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.header.CustodianHandler;
import dk.s4.hl7.cda.convert.decode.header.DocumentationOfHandler;
import dk.s4.hl7.cda.convert.decode.header.PatientHandler;
import dk.s4.hl7.cda.convert.decode.qrd.QuestionnaireTypeDocumentationOfHandler;
import dk.s4.hl7.cda.convert.decode.qrd.SectionHandler;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.qrd.QRDDocument;

/**
 * 
 * Parse QRD xml to GreenCDA model
 *
 */
public class XmlQRDConverter extends ClinicalDocumentXmlConverter<QRDDocument> {
  public QRDDocument convert(String source) {
    return deserialize(new StringReader(source));
  }

  @Override
  protected List<CDAXmlHandler<ClinicalDocument>> createParticipantHandlers() {
    List<CDAXmlHandler<ClinicalDocument>> list = new ArrayList<CDAXmlHandler<ClinicalDocument>>();
    list.add(new PatientHandler());
    list.add(createAuthorHandler());
    list.add(new CustodianHandler());
    list.add(createDataEntererHandler());
    list.add(createInformationRecipientHandler());
    list.add(createParticipantHandler());
    list.add(new DocumentationOfHandler());
    return list;
  }

  @Override
  protected QRDDocument createNewDocument(CDAHeaderHandler headerHandler) {
    return new QRDDocument(headerHandler.getDocumentId());
  }

  @Override
  protected List<CDAXmlHandler<QRDDocument>> createSectionHandlers() {
    List<CDAXmlHandler<QRDDocument>> list = new ArrayList<CDAXmlHandler<QRDDocument>>();
    list.add(new SectionHandler());
    list.add(new QuestionnaireTypeDocumentationOfHandler());
    return list;
  }
}
