package dk.s4.hl7.util.xml;

import java.io.IOException;
import java.io.Reader;

public class XmlRawTextReader extends Reader {
  private Reader reader;
  private Buffer buffer;

  public XmlRawTextReader(Reader reader) {
    this.reader = reader;
    this.buffer = new Buffer();
  }

  @Override
  public int read(char[] cbuf, int off, int len) throws IOException {
    int result = reader.read(cbuf, off, len);
    buffer.addToBuffer(cbuf, off, len, result);
    return result;
  }

  @Override
  public int read() throws IOException {
    int result = super.read();
    buffer.addToBuffer(new char[] { (char) result }, 0, 1, result);
    return result;
  }

  @Override
  public int read(char[] cbuf) throws IOException {
    int result = super.read(cbuf);
    buffer.addToBuffer(cbuf, 0, cbuf.length, result);
    return result;
  }

  @Override
  public void close() throws IOException {
    reader.close();
    buffer = null;
  }

  public void continuesCapture() {
    buffer.continuesCapture();
  }

  public String readRawText(int startIndex, int endIndex) {
    return buffer.readRawText(startIndex, endIndex);
  }

  private static class Buffer {
    private static final int BUILDER_BUFFER_CAPACITY = 8192;
    private StringBuilder builderBuffer;
    private boolean continueCapture;
    private int currentEndOffset;
    private int currentStartOffset;

    public Buffer() {
      this.currentEndOffset = 0;
      this.currentStartOffset = 0;
      builderBuffer = new StringBuilder(BUILDER_BUFFER_CAPACITY);
    }

    public void addToBuffer(char[] cbuf, int off, int len, int result) {
      if (result > 0) {
        if (continueCapture) {
          appendDataToBuffer(cbuf, off, len, result);
        } else {
          insertDataInBuffer(cbuf, off, len, result);
        }
      }
    }

    private void appendDataToBuffer(char[] cbuf, int off, int len, int result) {
      currentEndOffset += result;
      builderBuffer.append(cbuf, off, len);
    }

    private void insertDataInBuffer(char[] cbuf, int off, int len, int result) {
      currentStartOffset = currentEndOffset;
      currentEndOffset += result;
      builderBuffer.setLength(off);
      builderBuffer.append(cbuf, off, len);
    }

    public void continuesCapture() {
      continueCapture = true;
    }

    public String readRawText(int startIndex, int endIndex) {
      continueCapture = false;
      endIndex = findProperEndIndex(Math.abs(currentStartOffset - endIndex));
      startIndex = Math.abs(currentStartOffset - startIndex);
      if (startIndex >= endIndex) {
        // Occurs with <test/> and <test></test>
        return "";
      }
      return builderBuffer.substring(startIndex, endIndex);
    }

    private int findProperEndIndex(int endIndex) {
      endIndex--;
      while (builderBuffer.charAt(endIndex) != '<') {
        endIndex--;
      }
      return endIndex;
    }
  }
}
