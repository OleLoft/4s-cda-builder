package dk.s4.hl7.util.xml.hashing;

public abstract class HashableCharSequence implements CharSequence {
  protected final StringHasher hasher;

  public HashableCharSequence() {
    this.hasher = new StringHasher();
  }

  public HashableCharSequence(CharSequence text) {
    this.hasher = new StringHasher().appendHash(0, text);
  }

  public boolean equals(CharSequence text, Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj instanceof CharSequence) {
      if (this == obj) {
        return true;
      }
      CharSequence temp = (CharSequence) obj;
      if (temp.length() != text.length()) {
        return false;
      }
      for (int i = 0; i < text.length(); i++) {
        if (text.charAt(i) != temp.charAt(i)) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  protected boolean equalsIgnoreCase(CharSequence text1, CharSequence text2) {
    if (text2.length() != text1.length()) {
      return false;
    }
    for (int i = 0; i < text1.length(); i++) {
      char text1Char = text1.charAt(i);
      char text2Char = text2.charAt(i);
      if (Character.toUpperCase(text1Char) != Character.toUpperCase(text2Char)
          && Character.toLowerCase(text1Char) != Character.toLowerCase(text2Char)) {
        return false;
      }
    }
    return true;
  }

  public abstract boolean equalsIgnoreCase(CharSequence text);

  @Override
  public int hashCode() {
    return hasher.getHash();
  }
}
