package dk.s4.hl7.cda.convert.decode;

import static dk.s4.hl7.cda.convert.APDXmlCodec.Version.V10;
import static dk.s4.hl7.cda.convert.decode.TestXmlToApdUtil.validateStatusCode;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.APDXmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.decode.TestXmlToApdUtil.TestXmlToApd;
import dk.s4.hl7.cda.convert.encode.apd.SetupMedcomExample;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.StatusV10;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public class TestXmlToApdV10 extends BaseDecodeTest implements ConcurrencyTestCase, TestXmlToApd {

  private APDXmlCodec codec;

  @Before
  public void before() {
    setCodec(new APDXmlCodec(V10));
  }

  public void setCodec(APDXmlCodec codec) {
    this.codec = codec;
  }

  @Override
  public AppointmentDocument decode(String xml) {
    return super.decode(codec, xml);
  }

  @Override
  public String getStatusVersion() {
    return "StatusV10";
  }

  @Test
  public void testApd10XMLConverter() {
    String markup = null;
    try {
      markup = FileUtil.getData(this.getClass(), "apd/DK-APD_V10_Eksempel_default.markup.xml");
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }

    assertNotNull(markup);

    validateStatusCode(this, markup, "active");
    validateStatusCode(this, markup, "cancelled");
    validateStatusCode(this, markup, "suspended", true); // Expecting exception
    validateStatusCode(this, markup, "completed", true); // Expecting exception
    validateStatusCode(this, markup, "aborted", true); // Expecting exception

  }

  @Test
  public void testApdXMLConverter() {
    try {
      String XML = FileUtil.getData(this.getClass(), "apd/DK-APD_Example_1.xml");
      // AppointmentDocument a = decode(factory, XML);
      AppointmentDocument a = decode(codec, XML);
      a.getAuthor();
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testPHMRXmlConverterMedcomExampleV10() throws Exception {
    encodeDecodeAndCompare(new APDXmlCodec(V10), SetupMedcomExample.createBaseAppointmentDocument(StatusV10.ACTIVE));
  }

  @Override
  public void runTest() throws Exception {
    testPHMRXmlConverterMedcomExampleV10();
  }

}
