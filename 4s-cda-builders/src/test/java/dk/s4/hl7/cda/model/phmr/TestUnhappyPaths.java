package dk.s4.hl7.cda.model.phmr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.Patient.PatientBuilder;

/** Testing various boundary value cases of the
 * data classes.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class TestUnhappyPaths {

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void shouldHandlePersonWithOnlyNames() {
    Patient pi = new PatientBuilder("Hansen").addGivenName("Hans").build();
    String asString = pi.toString();
    assertNotNull(asString);
    assertEquals("PersonIdentity: G: Undifferentiated ID: None (Hansen,[Hans/] Prefix: null Birth: null Adr: (null))",
        asString);
  }
}
