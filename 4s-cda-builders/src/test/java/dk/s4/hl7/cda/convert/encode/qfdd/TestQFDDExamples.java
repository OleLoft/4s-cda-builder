package dk.s4.hl7.cda.convert.encode.qfdd;

import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.QFDDXmlCodec;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;

/**
 * Test QFDD Analog Slider example
 *
 * @author Frank Jacobsen, Systematic
 */
public final class TestQFDDExamples extends BaseEncodeTest<QFDDDocument> implements ConcurrencyTestCase {
  public TestQFDDExamples() {
    super("src/test/resources/qfdd");
    setCodec(new QFDDXmlCodec());
  }

  @Test
  public void testPreconditions() throws Exception {
    performGeneratedTest("generatedAnalogSliderWithPreconditions.xml", new SetupQFDDPrecondition().createDocument());
  }

  @Test
  public void testAnalogSlider() throws Exception {
    performGeneratedTest("generatedAnalogSliderTest.xml", new SetupQFDDAnalogSliderExample().createDocument());
  }

  @Test
  public void testDiscreteSlider() throws Exception {
    performGeneratedTest("generatedDiscreteSliderTest.xml", new SetupQFDDDiscreteSliderExample().createDocument());
  }

  @Test
  public void testMultipleChoice() throws Exception {
    performGeneratedTest("generatedMultipleChoiceTest.xml", new SetupQFDDMultipleChoiceExample().createDocument());
  }

  @Test
  public void testNumeric() throws Exception {
    performGeneratedTest("generatedNumericTest.xml", new SetupQFDDNumericExample().createDocument());
  }

  @Test
  public void testText() throws Exception {
    performGeneratedTest("generatedTextTest.xml", new SetupQFDDTextExample().createDocument());
  }

  @Override
  public void runTest() throws Exception {
    testAnalogSlider();
  }
}
