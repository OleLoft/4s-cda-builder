package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import dk.s4.hl7.cda.model.apd.AppointmentDocument;

public class TestXmlToApdUtil {

  interface TestXmlToApd {
    public AppointmentDocument decode(String xml);

    public String getStatusVersion();
  }

  protected static void validateStatusCode(TestXmlToApd testXmlToApd, String markup, String code) {
    validateStatusCode(testXmlToApd, markup, code, false);
  }

  protected static void validateStatusCode(TestXmlToApd testXmlToApd, String markup, String code,
      boolean expectException) {
    String xml;
    AppointmentDocument appointment;

    try {
      xml = markup.replace("#*#statusCode#*#", code.toLowerCase());
      appointment = testXmlToApd.decode(xml);
      if (expectException) {
        fail("Expected an Exception to be thrown when checking statuscode:" + code);
      }
      assertEquals(code.toUpperCase(), appointment.getAppointmentStatus().toString());
    } catch (CdaBuilderException exception) {
      if (!expectException) {
        fail("Did not expect an Exception to be thrown when checking statuscode:" + code);
      }

      assertEquals("Cant parse xml document No enum constant dk.s4.hl7.cda.model.apd." + testXmlToApd.getStatusVersion()
          + "." + code.toUpperCase(), exception.getMessage());
    }
  }

}
