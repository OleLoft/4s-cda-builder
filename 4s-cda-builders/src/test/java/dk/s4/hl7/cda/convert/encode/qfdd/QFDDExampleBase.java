package dk.s4.hl7.cda.convert.encode.qfdd;

import java.util.Date;
import java.util.UUID;

import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.encode.CDAExample;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDCriterion.QFDDCriterionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition.QFDDPreconditionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup.GroupType;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup.QFDDPreconditionGroupBuilder;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public abstract class QFDDExampleBase implements CDAExample<QFDDDocument> {

  protected QFDDDocument createBaseQFDDDocument() {
    return createBaseQFDDDocument(UUID.randomUUID().toString());
  }

  protected QFDDDocument createBaseQFDDDocument(String documentId) {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);
    // Create document
    QFDDDocument qfddDocument = new QFDDDocument(MedCom.createId(documentId));
    qfddDocument.setLanguageCode("da-DK");
    qfddDocument.setTitle("KOL spørgeskema");
    qfddDocument.setDocumentVersion("2358344", 1);
    qfddDocument.setEffectiveTime(documentCreationTime);
    // Create Patient
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    qfddDocument.setPatient(nancy);
    // Create Custodian organization
    OrganizationIdentity custodianOrganization = new OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();
    qfddDocument.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    PersonIdentity andersAndersen = new PersonBuilder("Andersen").addGivenName("Anders").build();

    qfddDocument.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    // 1.4 Define the service period
    Date from = DateUtil.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = DateUtil.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    qfddDocument.setDocumentationTimeInterval(from, to);
    return qfddDocument;
  }

  protected QFDDPrecondition simplePrecondition() {
    CodedValue questionCodedValue = new CodedValue("value1", "value2", "value3", "value4");
    return new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
        .setMaximum("20")
        .setMinimum("10")
        .setValueType(IntervalType.IVL_INT)
        .build()).build();
  }

  protected QFDDPrecondition groupedPrecondition() {
    CodedValue questionCodedValue = new CodedValue("value1", "value2", "value3", "value4");
    CodedValue conjunctionCode = new CodedValue("con1", "con2", "con3", "con4");
    return new QFDDPreconditionBuilder(new QFDDPreconditionGroupBuilder()
        .setGroupType(GroupType.AtLeastOneTrue)
        .setId(MedCom.createId("simplePrecondition"))
        .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
            .setMaximum("20")
            .setMinimum("10")
            .setValueType(IntervalType.IVL_INT)
            .build()).setConjunctionCode(conjunctionCode).setNegationInd(true).build())
        .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
            .setMaximum("30")
            .setMinimum("20")
            .setValueType(IntervalType.IVL_INT)
            .build()).setConjunctionCode(conjunctionCode).setNegationInd(true).build())
        .addPrecondition(new QFDDPreconditionBuilder(
            new QFDDCriterionBuilder(questionCodedValue).setAnswer(questionCodedValue).build()).build())
        .build()).build();
  }

  protected QFDDPrecondition preconditionWithMultipleGroups() {
    return new QFDDPreconditionBuilder(new QFDDPreconditionGroupBuilder()
        .setGroupType(GroupType.AllTrue)
        .setId(MedCom.createId("veryComplexPrecondition"))
        .addPrecondition(groupedPrecondition())
        .addPrecondition(groupedPrecondition())
        .build()).build();
  }
}
