package dk.s4.hl7.cda.convert;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.convert.decode.TestXmlToApdV10;
import dk.s4.hl7.cda.convert.decode.TestXmlToPhmr;
import dk.s4.hl7.cda.convert.decode.TestXmlToQfdd;
import dk.s4.hl7.cda.convert.decode.TestXmlToQrd;
import dk.s4.hl7.cda.convert.encode.apd.TestAgainstMedComApdExamples;
import dk.s4.hl7.cda.convert.encode.phmr.TestAgainstMedComPhmrExamples;
import dk.s4.hl7.cda.convert.encode.qfdd.TestQFDDExamples;
import dk.s4.hl7.cda.convert.encode.qrd.TestQRDExamples;
import dk.s4.hl7.cda.model.core.ClinicalDocument;

public class ConcurrencyTest {
  private static final int NOF_THREADS = Runtime.getRuntime().availableProcessors() * 2;
  private static final ExecutorService service = Executors.newFixedThreadPool(NOF_THREADS);
  private static final int ITERATIONS_PER_THREAD = 150;
  private static final Logger logger = LoggerFactory.getLogger(ConcurrencyTest.class);

  @Before
  public void before() {
    //Assume.assumeTrue(System.getProperty("concurrency_test") != null);
  }

  @Test
  public void testQRDCodec_XmlToQrd() throws Exception {
    logger.info("Performing concurrency test: XmlToQrd");
    QRDXmlCodec codec = new QRDXmlCodec();
    TestXmlToQrd testCase = new TestXmlToQrd();
    testCase.setCodec(codec);
    runTest(testCase);
  }

  @Test
  public void testQFDDCodec_XmlToQfdd() throws Exception {
    logger.info("Performing concurrency test: XmlToQfdd");
    QFDDXmlCodec codec = new QFDDXmlCodec();
    TestXmlToQfdd testCase = new TestXmlToQfdd();
    testCase.setCodec(codec);
    runTest(testCase);
  }

  @Test
  public void testPHMRCodec_XmlToPhmr() throws Exception {
    logger.info("Performing concurrency test: XmlToPhmr");
    PHMRXmlCodec codec = new PHMRXmlCodec();
    TestXmlToPhmr testCase = new TestXmlToPhmr();
    testCase.setCodec(codec);
    runTest(testCase);
  }

  @Test
  public void testAPDCodec_XmlToApd() throws Exception {
    logger.info("Performing concurrency test: XmlToApd");
    APDXmlCodec codec = new APDXmlCodec();
    TestXmlToApdV10 testCase = new TestXmlToApdV10();
    testCase.setCodec(codec);
    runTest(testCase);
  }

  @Test
  public void testQRDCodec_QrdToXml() throws Exception {
    logger.info("Performing concurrency test: QrdToXml");
    QRDXmlCodec codec = new QRDXmlCodec();
    TestQRDExamples testCase = new TestQRDExamples();
    testCase.setCodec(codec);
    runTest(testCase);
  }

  @Test
  public void testQFDDCodec_QfddToXml() throws Exception {
    logger.info("Performing concurrency test: QfddToXml");
    QFDDXmlCodec codec = new QFDDXmlCodec();
    TestQFDDExamples testCase = new TestQFDDExamples();
    testCase.setCodec(codec);
    runTest(testCase);
  }

  @Test
  public void testPHMRCodec_PhmrToXml() throws Exception {
    logger.info("Performing concurrency test: PhmrToXml");
    PHMRXmlCodec codec = new PHMRXmlCodec();
    TestAgainstMedComPhmrExamples testCase = new TestAgainstMedComPhmrExamples();
    testCase.setCodec(codec);
    runTest(testCase);
  }

  @Test
  public void testAPDCodec_ApdToXml() throws Exception {
    logger.info("Performing concurrency test: ApdToXml");
    APDXmlCodec codec = new APDXmlCodec();
    TestAgainstMedComApdExamples testCase = new TestAgainstMedComApdExamples();
    testCase.setCodec(codec);
    runTest(testCase);
  }

  private <E extends ClinicalDocument> void runTest(ConcurrencyTestCase testCase) throws Exception {
    List<MyCallable<E>> tasks = new ArrayList<MyCallable<E>>(NOF_THREADS);
    for (int i = 0; i < NOF_THREADS; i++) {
      tasks.add(new MyCallable<E>(testCase));
    }
    for (Future<Exception> fex : service.invokeAll(tasks)) {
      if (fex.get() != null) {
        throw fex.get();
      }
    }
  }

  private static class MyCallable<E extends ClinicalDocument> implements Callable<Exception> {
    private ConcurrencyTestCase testCase;

    public MyCallable(ConcurrencyTestCase testCase) {
      super();
      this.testCase = testCase;
    }

    @Override
    public Exception call() throws Exception {
      try {
        for (int i = 0; i < ITERATIONS_PER_THREAD; i++) {
          testCase.runTest();
        }
      } catch (Exception ex) {
        return ex;
      }
      return null;
    }
  }
}
