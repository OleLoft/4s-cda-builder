package dk.s4.hl7.cda.convert.encode.qfdd;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDFeedback.QFDDFeedbackBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDHelpText.QFDDHelpTextBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion.QFDDTextQuestionBuilder;

/**
 * Test QFDD Text example
 *
 * @author Frank Jacobsen, Systematic
 * 
 */
public final class SetupQFDDTextExample extends QFDDExampleBase {
  private static final String QUESTION = "question";
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  protected QFDDQuestion simpleQuestion() {
    return new QFDDTextQuestionBuilder()
        .setCodeValue(new CodedValue("value1", "value2", "value3", "value4"))
        .setId(MedCom.createId("idExtension1"))
        .setQuestion(QUESTION)
        .build();
  }

  protected QFDDQuestion fullQuestion() {
    return new QFDDTextQuestionBuilder()
        .setCodeValue(new CodedValue("value1", "value2", "value3", "value4"))
        .setId(MedCom.createId("idExtension1"))
        .setQuestion(QUESTION)
        .setFeedback(new QFDDFeedbackBuilder().feedBackText("feedbacktext1").language("da-DK").build())
        .setHelpText(new QFDDHelpTextBuilder().helpText("helptext").language("da-DK").build())
        .build();
  }

  public QFDDDocument createDocument() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);

    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDQuestion> section = new Section<QFDDQuestion>("Indledning", text);
    section.addQuestionnaireEntity(simpleQuestion());
    section.addQuestionnaireEntity(fullQuestion());
    cda.addSection(section);
    return cda;
  }
}
