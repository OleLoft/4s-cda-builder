package dk.s4.hl7.cda.convert.encode.apd;

import java.util.Date;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.Status;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public class SetupMedcomExample {

  private static final String DOCUMENT_ID = "aa2386d0-79ea-11e3-981f-0800200c9a66";

  public static AppointmentDocument createBaseAppointmentDocument(Status staus) {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTime(2017, 0, 13, 10, 0, 0);

    Date authorTime = DateUtil.makeDanishDateTime(2017, 1, 16, 10, 0, 0);

    // Create document
    AppointmentDocument appointment = new AppointmentDocument(MedCom.createId(DOCUMENT_ID));
    appointment.setLanguageCode("da-DK");
    appointment.setTitle("Aftale for 2512489996");
    appointment.setEffectiveTime(documentCreationTime);
    // Create Patient
    Patient nancy = Setup.defineNancyAsFullPersonIdentity("2512489996");
    appointment.setPatient(nancy);
    // Create Custodian organization
    OrganizationIdentity custodianOrganization = new OrganizationBuilder()
        .setSOR("515361000016007")
        .setName("OUH Klinisk IT (Odense)")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("J. B. Winsløwsvej 4 1")
            .setCity("Odense C")
            .setPostalCode("5000")
            .setCountry("Danmark")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "66113333-2")
        .build();
    appointment.setCustodian(custodianOrganization);

    OrganizationIdentity authorOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("242621000016001")
        .setName("OUH Radiologisk Afdeling (Svendborg)")
        .setAddress(Setup.defineValdemarsGade53Address())
        .addTelecom(Use.WorkPlace, "tel", "65113333-1")
        .build();

    PersonIdentity jensJensen = new PersonBuilder("Jensen").addGivenName("Jens").setPrefix("Læge").build();
    PersonIdentity andersAndersen = new PersonBuilder("Andersen").addGivenName("Anders").build();
    PersonIdentity laegeAndersAndersen = new PersonBuilder("Andersen").addGivenName("Anders").setPrefix("Læge").build();

    appointment.setAuthor(new ParticipantBuilder()
        .setAddress(authorOrganization.getAddress())
        .setSOR(authorOrganization.getIdValue())
        .setTelecomList(authorOrganization.getTelecomList())
        .setTime(authorTime)
        .setPersonIdentity(jensJensen)
        .setOrganizationIdentity(authorOrganization)
        .build());

    // 1.4 Define the service period       
    Date from = DateUtil.makeDanishDateTime(2017, 4, 31, 11, 0, 0);
    Date to = DateUtil.makeDanishDateTime(2017, 4, 31, 12, 0, 0);

    appointment.setDocumentationTimeInterval(from, to);

    OrganizationIdentity appointmentLocation = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("320161000016005")
        .setName("OUH Radiologisk Ambulatorium (Nyborg)")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Vestergade 17")
            .setCity("Nyborg")
            .setPostalCode("5800")
            .setCountry("Danmark")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "66113333-4")
        .build();

    appointment.setAppointmentTitle("Aftale");
    appointment.setAppointmentText("<paragraph>Aftale:</paragraph>" + "<table width=\"100%\">" + "<tbody>" + "<tr>"
        + "<th>Status</th>" + "<th>Aftale dato</th>" + "<th>Vedrørende</th>" + "<th>Mødested</th>" + "</tr>" + "<tr>"
        + "<td>active</td>" + "<td>2017-05-31 11:00 - 2017-05-31 11:20</td>"
        + "<td>Ekkokardiografi (Ultralydsundersøgelse af hjertet)</td>" + "<td>Vestergade 17, 5800 Nyborg</td>"
        + "</tr>" + "</tbody>" + "</table>");
    appointment.setAppointmentId("9a6d1bac-17d3-4195-89a4-1121bc809b4d");
    appointment.setAppointmentStatus(staus);
    appointment.setIndicationDisplayName("Ekkokardiografi (Ultralydsundersøgelse af hjertet)");
    appointment.setAppointmentLocation(appointmentLocation);

    Participant appointmentAuthor = new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Toldbodvej 9")
            .setCity("Svendborg")
            .setPostalCode("5700")
            .setCountry("Danmark")
            .setUse(Use.WorkPlace)
            .build())
        .setSOR("48681000016007")
        .addTelecom(Use.WorkPlace, "tel", "62214518")
        .setTime(authorTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder().setName("Lægerne Toldbodvej").build())
        .build();

    appointment.setAppointmentAuthor(appointmentAuthor);

    Participant appointmentPerformer = new ParticipantBuilder()
        .setAddress(Setup.defineValdemarsGade53Address())
        .setSOR("320161000016005")
        .addTelecom(Use.WorkPlace, "tel", "66113333-3")
        .setTime(authorTime)
        .setPersonIdentity(laegeAndersAndersen)
        .setOrganizationIdentity(
            new OrganizationIdentity.OrganizationBuilder().setName("OUH Radiologisk Ambulatorium (Nyborg)").build())
        .build();

    appointment.setAppointmentPerformer(appointmentPerformer);

    return appointment;
  }

}
