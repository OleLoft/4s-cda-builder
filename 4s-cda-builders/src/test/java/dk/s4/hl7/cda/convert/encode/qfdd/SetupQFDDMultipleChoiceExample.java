package dk.s4.hl7.cda.convert.encode.qfdd;

import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qfdd.QFDDCriterion.QFDDCriterionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDFeedback.QFDDFeedbackBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDHelpText.QFDDHelpTextBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion.QFDDMultipleChoiceQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition.QFDDPreconditionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;

/**
 * Test QFDD Multible Choise example
 *
 * @author Frank Jacobsen, Systematic
 * 
 */
public final class SetupQFDDMultipleChoiceExample extends QFDDExampleBase {
  private static final String QUESTION = "question";
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  protected QFDDQuestion simpleQuestion() {
    return new QFDDMultipleChoiceQuestionBuilder()
        .setInterval(1, 2)
        .setCodeValue(new CodedValue("value1", "value2", "value3", "value4"))
        .setId(MedCom.createId("idExtension1"))
        .addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited", "Some-CodeSystem-Name")
        .setQuestion(QUESTION)
        .build();
  }

  protected QFDDQuestion simpleQuestionWithReferenceAndPrecondition() {
    CodedValue questionCodedValue = new CodedValue("value1", "value2", "value3", "value4");
    return new QFDDMultipleChoiceQuestionBuilder()
        .setInterval(0, 2)
        .setCodeValue(questionCodedValue)
        .setId(MedCom.createId("idExtension1"))
        .addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited", "Some-CodeSystem-Name")
        .setQuestion(QUESTION)
        .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
            .setMaximum("20")
            .setMinimum("0")
            .setValueType(IntervalType.IVL_INT)
            .build()).build())
        .build();
  }

  protected QFDDQuestion fullQuestion() {
    CodedValue questionCodedValue = new CodedValue("value1", "value2", "value3", "value4");
    return new QFDDMultipleChoiceQuestionBuilder()
        .setInterval(1, 2)
        .setCodeValue(questionCodedValue)
        .setId(MedCom.createId("idExtension1"))
        .addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited", "Some-CodeSystem-Name")
        .setQuestion(QUESTION)
        .setFeedback(new QFDDFeedbackBuilder()
            .feedBackText("feedbacktext1")
            .language("da-DK")
            .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
                .setMaximum("20")
                .setMinimum("1")
                .setValueType(IntervalType.IVL_INT)
                .build()).build())
            .build())
        .setHelpText(new QFDDHelpTextBuilder().helpText("helptext").language("da-DK").build())
        .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
            .setMaximum("20")
            .setMinimum("1")
            .setValueType(IntervalType.IVL_INT)
            .build()).build())
        .build();
  }

  public QFDDDocument createDocument() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);

    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDQuestion> section = new Section<QFDDQuestion>("Indledning", text);
    section.addQuestionnaireEntity(simpleQuestion());
    section.addQuestionnaireEntity(simpleQuestionWithReferenceAndPrecondition());
    section.addQuestionnaireEntity(fullQuestion());
    cda.addSection(section);
    return cda;
  }
}
