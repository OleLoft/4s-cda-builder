<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>dk.s4.hl7</groupId>
	<artifactId>builders</artifactId>
	<version>2.0.0</version>

	<packaging>jar</packaging>

	<name>4S CDA Builders</name>
	<description>CDA Builders supporting the Danish profiles</description>

	<dependencies>
		<!-- setup slf4j -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4jVersion}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.4</version>
		</dependency>
		<!-- Test dependencies -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>xmlunit</groupId>
			<artifactId>xmlunit</artifactId>
			<version>1.4</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.connectopensource</groupId>
			<artifactId>CONNECTCommonTypesLib</artifactId>
			<version>4.0.7</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>${slf4jVersion}</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<properties>
		<!-- Source file encoding -->
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<slf4jVersion>1.7.7</slf4jVersion>
	</properties>

	<build>
		<plugins>
			<!-- Java version -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
			<!-- Eclipse settings -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<version>2.9</version>
				<configuration>
					<additionalConfig>
						<!-- Hack to set source encoding in eclipse -->
						<file>
							<name>.settings/org.eclipse.core.resources.prefs</name>
							<content>
                <![CDATA[eclipse.preferences.version=1${line.separator}encoding/<project>=${project.build.sourceEncoding}${line.separator}]]>
							</content>
						</file>
					</additionalConfig>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.0.2</version>
				<executions>
					<execution>
						<goals>
							<goal>test-jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<!-- Plugins for generating jars containing source code and javadoc -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.3</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.9.1</version>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<configuration>
							<failOnError>false</failOnError>
						</configuration>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>license-maven-plugin</artifactId>
				<version>1.12</version>
				<dependencies>
					<dependency>
						<groupId>org.apache.maven.doxia</groupId>
						<artifactId>doxia-core</artifactId>
						<version>1.4</version>
					</dependency>
				</dependencies>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<version>2.9</version>
				<executions>
					<execution>
						<goals>
							<goal>dependencies</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>net.revelc.code.formatter</groupId>
										<artifactId>formatter-maven-plugin</artifactId>
										<versionRange>[2.0.0,)</versionRange>
										<goals>
											<goal>format</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore />
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
	<profiles>
		<profile>
			<activation>
				<activeByDefault>false</activeByDefault>
				<jdk>[1.7,)</jdk> <!-- JDK version 1.7 and above -->
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>net.revelc.code.formatter</groupId>
						<artifactId>formatter-maven-plugin</artifactId>
						<version>2.0.0</version>
						<configuration>
							<configFile>${project.basedir}/eclipse-profile.xml</configFile>
						</configuration>
						<executions>
							<execution>
								<id>format</id>
								<phase>compile</phase>
								<goals>
									<goal>format</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
	<distributionManagement>
		<snapshotRepository>
			<id>4S-artifactory-snapshots</id>
			<name>4S Artifactory SNAPSHOTS</name>
			<url>http://artifactory.4s-online.dk/artifactory/libs-snapshot-local</url>
		</snapshotRepository>
		<repository>
			<id>4S-artifactory-releases</id>
			<name>4S Artifactory RELEASES</name>
			<url>http://artifactory.4s-online.dk/artifactory/libs-release-local</url>
		</repository>
	</distributionManagement>
</project>
