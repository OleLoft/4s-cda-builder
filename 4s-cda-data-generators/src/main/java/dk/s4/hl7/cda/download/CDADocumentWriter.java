package dk.s4.hl7.cda.download;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType.DocumentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CDADocumentWriter {
  private static final Logger logger = LoggerFactory.getLogger(CDADocumentWriter.class);
  private File outputFolder;
  private String encoding;

  public CDADocumentWriter(File outputFolder, String encoding) {
    this.outputFolder = outputFolder;
    this.encoding = encoding;
  }

  public void write(List<DocumentResponse> documents) {
    logger.info(String.format("Writing %s downloaded documents to files in output folder", documents.size()));
    for (DocumentResponse documentResponse : documents) {
      try {
        File file = createOutputFile(documentResponse, outputFolder);
        Writer writer = new OutputStreamWriter(new FileOutputStream(file), encoding);
        writer.write(new String(documentResponse.getDocument()));
        writer.flush();
        writer.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
    logger.info("Writing documents complete");
  }

  private File createOutputFile(DocumentResponse document, File parentFolder) throws IOException {
    String fileName = document.getDocumentUniqueId();
    fileName = fileName.replace('.', '_');
    fileName = fileName.replace('^', '_');
    File file = new File(parentFolder, String.format("%s.xml", fileName));
    if (file.exists()) {
      file.delete();
    }
    file.createNewFile();
    return file;
  }
}
