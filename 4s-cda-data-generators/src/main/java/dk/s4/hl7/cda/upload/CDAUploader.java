package dk.s4.hl7.cda.upload;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.net4care.xdsconnector.IRepositoryConnector;
import org.net4care.xdsconnector.RepositoryConnector;
import org.net4care.xdsconnector.SendRequestException;
import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.Utilities.PrepareRequestException;
import org.net4care.xdsconnector.service.ObjectFactory;
import org.net4care.xdsconnector.service.RegistryResponseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = { "org.net4care" })
@EnableAutoConfiguration
public class CDAUploader {
  private static final Logger logger = LoggerFactory.getLogger(CDAUploader.class);
  private static final int[] RETRY_TIMEOUT_LIST = new int[] { 3, 3, 3, 300 };

  private static final Option OPTION_PROPERTIES = Option
      .builder("properties")
      .desc("Path to the file of upload.properties")
      .hasArg()
      .required()
      .build();

  private static final Option OPTION_INPUT_FOLDER = Option
      .builder("inputfolder")
      .hasArg()
      .desc("Path to the folder where the CDA xml document resides")
      .required()
      .build();

  private static final Option OPTION_RETRY = Option
      .builder("retry")
      .desc("Flag to activate retry strategy")
      .hasArg(false)
      .required(false)
      .build();

  private static final Option OPTION_DELAY = Option
      .builder("uploadDelay")
      .desc("delay between uploads in ms")
      .hasArg(false)
      .required(false)
      .build();

  private static final JAXBContext jaxbContext = createJAXBContext();

  private static JAXBContext createJAXBContext() {
    try {
      return JAXBContext.newInstance(RegistryResponseType.class);
    } catch (JAXBException e) {
      throw new RuntimeException(e);
    }
  }

  public static void main(String[] args) {
    try {
      CommandLine cmdLine = parseArguments(args);
      Properties properties = loadPropertiesFile(cmdLine.getOptionValue(OPTION_PROPERTIES.getOpt()));
      List<File> inputCDAFiles = findCDAInputFiles(
          checkInputFolder(cmdLine.getOptionValue(OPTION_INPUT_FOLDER.getOpt())));

      if (inputCDAFiles.isEmpty()) {
        logger.info(
            "No documents to download from input folder: " + cmdLine.getOptionValue(OPTION_INPUT_FOLDER.getOpt()));
        return;
      }
      logger.info("Starting XDSConnector");
      ConfigurableApplicationContext run = new SpringApplicationBuilder()
          .main(CDAUploader.class)
          .sources(CDAUploader.class)
          .logStartupInfo(false)
          .showBanner(false)
          .properties(properties)
          .run(args);
      IRepositoryConnector xdsRepositoryConnector = run.getBean("xdsRepositoryConnector", RepositoryConnector.class);
      logger.info("XDSConnector started");

      int submissionBatchSize = Integer.valueOf(readProperty(properties, "xds.submissionSet.batchsize"));
      xdsRepositoryConnector = useDelayOption(cmdLine, properties, xdsRepositoryConnector, submissionBatchSize);
      uploadCDADocumentsLoop(cmdLine, properties, inputCDAFiles, xdsRepositoryConnector, submissionBatchSize);
      logger.info("Upload was successful");
    } catch (CDAUploaderException ex) {
      logger.error(ex.getMessage());
    } catch (Exception ex) {
      logger.error(ex.getMessage(), ex);
    }
  }

  private static void uploadCDADocumentsLoop(CommandLine cmdLine, Properties properties, List<File> inputCDAFiles,
      IRepositoryConnector xdsRepositoryConnector, int submissionBatchSize) {
    for (int startindex = 0; startindex < inputCDAFiles.size(); startindex += submissionBatchSize) {
      int endIndex = determineBatchEndIndex(inputCDAFiles, startindex, submissionBatchSize);
      logger.info(String.format("Uploading documents: %s to %s of %s", startindex + 1, endIndex, inputCDAFiles.size()));
      List<String> cdaDocuments = readSelectionOfCDADocuments(inputCDAFiles, startindex,
          startindex + submissionBatchSize, submissionBatchSize);
      if (cmdLine.hasOption(OPTION_RETRY.getOpt())) {
        retryUploadCDADocumentsMultiple(properties, xdsRepositoryConnector, cdaDocuments);
      } else {
        uploadCDADocumentsMultiple(properties, xdsRepositoryConnector, cdaDocuments);
      }
    }
  }

  private static IRepositoryConnector useDelayOption(CommandLine cmdLine, Properties properties,
      IRepositoryConnector xdsRepositoryConnector, int submissionBatchSize) {
    if (cmdLine.hasOption(OPTION_DELAY.getOpt())) {
      int sleepDuration = Integer.valueOf(readProperty(properties, "xds.submissionSet.sleeptime"));
      return new SleepyRespositoryDecorator(submissionBatchSize, sleepDuration, xdsRepositoryConnector);
    }
    return xdsRepositoryConnector;
  }

  private static int determineBatchEndIndex(List<File> inputCDAFiles, int startindex, int submissionBatchSize) {
    int endIndex = startindex + submissionBatchSize;
    return endIndex > inputCDAFiles.size() ? inputCDAFiles.size() : endIndex;
  }

  private static void retryUploadCDADocumentsMultiple(Properties properties,
      IRepositoryConnector xdsRepositoryConnector, List<String> cdaDocuments) {
    long start = System.currentTimeMillis();
    IRepositoryConnector wrappedRepositoryConnector = new MultipleSubmissionSetRepositoryDecorator(
        new RetryRepositoryDecorator(RETRY_TIMEOUT_LIST, xdsRepositoryConnector));

    RegistryResponseType response = null;
    try {
      response = wrappedRepositoryConnector.provideAndRegisterCDADocuments(cdaDocuments,
          loadHealthcareFacilityType(properties), loadPracticeSettingsCode(properties));
    } catch (PrepareRequestException | SendRequestException e) {
      throw new CDAUploaderException(
          "The Repository ProvideAndRegisterDocumentSet was unsuccessful due to: " + e.getMessage());
    }
    long stop = System.currentTimeMillis();
    checkResponse(response);
    logger.info("Upload duration for " + cdaDocuments.size() + " document(s): " + (stop - start) + ", "
        + (cdaDocuments.size() * 1000) / (stop - start) + " requests per second");
  }

  private static void uploadCDADocumentsMultiple(Properties properties, IRepositoryConnector xdsRepositoryConnector,
      List<String> cdaDocuments) {
    long start = System.currentTimeMillis();
    IRepositoryConnector wrappedRepositoryConnector = new MultipleSubmissionSetRepositoryDecorator(
        xdsRepositoryConnector);
    RegistryResponseType response = null;
    try {
      response = wrappedRepositoryConnector.provideAndRegisterCDADocuments(cdaDocuments,
          loadHealthcareFacilityType(properties), loadPracticeSettingsCode(properties));
    } catch (PrepareRequestException | SendRequestException e) {
      throw new CDAUploaderException(
          "The Repository ProvideAndRegisterDocumentSet was unsuccessful due to: " + e.getMessage());
    }
    long stop = System.currentTimeMillis();
    checkResponse(response);
    logger.info("Upload duration for " + cdaDocuments.size() + " document(s): " + (stop - start) + ", "
        + (cdaDocuments.size() * 1000) / (stop - start) + " requests per second");
  }

  private static void checkResponse(RegistryResponseType response) {
    if (response == null || !RetryRepositoryDecorator.REQUEST_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
      logResponse(response);
      throw new CDAUploaderException(
          "The Repository ProvideAndRegisterDocumentSet was unsuccessful - Please look in the above response");
    }
  }

  private static void logResponse(RegistryResponseType repositoryReponse) {
    try {
      StringWriter writer = new StringWriter();
      writer.append("Repository response: ");
      jaxbContext.createMarshaller().marshal(new ObjectFactory().createRegistryResponse(repositoryReponse), writer);
      logger.info(writer.toString());
    } catch (Exception ex) {
      throw new CDAUploaderException(ex);
    }

  }

  private static CodedValue loadPracticeSettingsCode(Properties properties) {
    String code = readProperty(properties, "practicesettingscode.code");
    String codeSystem = readProperty(properties, "practicesettingscode.codesystem");
    String displayName = readProperty(properties, "practicesettingscode.displayname");
    return new CodedValue.CodedValueBuilder()
        .setCode(code)
        .setCodeSystem(codeSystem)
        .setDisplayName(displayName)
        .build();
  }

  private static CodedValue loadHealthcareFacilityType(Properties properties) {
    String code = readProperty(properties, "healthcarefacilitytype.code");
    String codeSystem = readProperty(properties, "healthcarefacilitytype.codesystem");
    String displayName = readProperty(properties, "healthcarefacilitytype.displayname");
    return new CodedValue.CodedValueBuilder()
        .setCode(code)
        .setCodeSystem(codeSystem)
        .setDisplayName(displayName)
        .build();
  }

  private static String readProperty(Properties properties, String propertyName) {
    String property = properties.getProperty(propertyName);
    if (property == null || property.trim().isEmpty()) {
      throw new RuntimeException(String.format("The property '%s' is missing from the properties file", propertyName));
    }
    return property;
  }

  private static List<String> readSelectionOfCDADocuments(List<File> inputCDAFiles, int startIndex, int endIndex,
      int submissionBatchSize) {
    List<String> cdaDocuments = new ArrayList<String>(submissionBatchSize);
    for (int i = startIndex; i < inputCDAFiles.size() && i < endIndex; i++) {
      cdaDocuments.add(loadFile(inputCDAFiles.get(i)));
    }
    return cdaDocuments;
  }

  private static String loadFile(File cdaFile) {
    InputStreamReader reader = null;
    char[] charBuffer = new char[(int) cdaFile.length()];
    int endIndex = -1;
    try {
      reader = new InputStreamReader(new FileInputStream(cdaFile), "UTF-8");
      endIndex = reader.read(charBuffer);
    } catch (Exception ex) {
      logger.info(String.format("Could not read file '%s' because of: %s", cdaFile.getAbsolutePath(), ex.getMessage()));
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (Exception ex) {
          // Ignore
        }
      }
    }
    if (endIndex > 0) {
      return new String(charBuffer, 0, endIndex);
    }
    return "";
  }

  private static CommandLine parseArguments(String[] args) throws ParseException {
    Options options = new Options();
    options.addOption(OPTION_PROPERTIES);
    options.addOption(OPTION_INPUT_FOLDER);
    options.addOption(OPTION_RETRY);
    options.addOption(OPTION_DELAY);
    CommandLineParser cmdParser = new DefaultParser();
    return cmdParser.parse(options, args);
  }

  private static List<File> findCDAInputFiles(File inputFolder) {
    File[] files = inputFolder.listFiles();
    List<File> inputCDAFiles = new ArrayList<File>(files.length);
    for (File file : files) {
      if (file.isDirectory()) {
        inputCDAFiles.addAll(findCDAInputFiles(file)); // Recursive call
      } else if (file.getAbsolutePath().toLowerCase().endsWith(".xml")) {
        inputCDAFiles.add(file);
      } else {
        logger.info("Ignoring non-xml file at CDA input folder: " + file.getAbsolutePath());
      }
    }
    return inputCDAFiles;
  }

  private static Properties loadPropertiesFile(String propertiesFileName) throws IOException {
    File file = new File(propertiesFileName);
    if (!file.exists()) {
      throw new RuntimeException("Properties file does not exist: " + file.getAbsolutePath());
    }
    if (!file.isFile()) {
      throw new RuntimeException("Properties file is not a file: " + file.getAbsolutePath());
    }
    if (!file.getAbsolutePath().endsWith(".properties")) {
      throw new RuntimeException("Properties file does not end with '.properties': " + file.getAbsolutePath());
    }
    Properties properties = new Properties();
    Reader reader = null;
    try {
      reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
      properties.load(reader);
    } finally {
      if (reader != null) {
        reader.close();
      }
    }
    return properties;
  }

  private static File checkInputFolder(String inputFolderPath) throws IOException {
    File file = new File(inputFolderPath);
    if (!file.exists()) {
      throw new RuntimeException("Input folder does not exist: " + file.getAbsolutePath());
    }
    if (!file.isDirectory()) {
      throw new RuntimeException("Input folder is not a folder: " + file.getAbsolutePath());
    }
    return file;
  }
}
