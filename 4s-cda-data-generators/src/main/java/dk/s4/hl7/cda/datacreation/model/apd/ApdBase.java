package dk.s4.hl7.cda.datacreation.model.apd;

public class ApdBase extends ApdCsvObject {
  private String appointmentId;
  private String appointmentStatus;
  private String appointmentCreationDate;

  public String getAppointmentId() {
    return appointmentId;
  }

  public void setAppointmentId(String appointmentId) {
    this.appointmentId = appointmentId;
  }

  public String getAppointmentStatus() {
    return appointmentStatus;
  }

  public void setAppointmentStatus(String appointmentStatus) {
    this.appointmentStatus = appointmentStatus;
  }

  public String getAppointmentCreationDate() {
    return appointmentCreationDate;
  }

  public void setAppointmentCreationDate(String appointmentCreationDate) {
    this.appointmentCreationDate = appointmentCreationDate;
  }

  @Override
  public boolean isObjectEmpty() {
    return false;
  }

  @Override
  public String toCsv() {
    return this.appointmentId + ";" + this.appointmentStatus + ";" + this.appointmentCreationDate + ";";
  }
}
