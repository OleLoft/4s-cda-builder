import groovy.io.FileType;

def parameters = "-patient 0211223989 -approved"

File file = new File("./");

def runProcessStep = { File mainName ->
	def process = ("java -cp ${mainName.getName()} dk.s4.hl7.cda.download.CDADownloader " + parameters).execute(null, mainName.getParentFile());
	process.in.eachLine { line -> println line; }
	process.err.eachLine { line -> println line; }
}

file.traverse(type: FileType.FILES, nameFilter: ~/.*-jar-with-dependencies\.jar/) {
	println "Found file ${it.getName()}"
	runProcessStep.call(it);
	return;
}
